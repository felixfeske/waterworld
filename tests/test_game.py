import time
from Game import Game, GameState
from Map import HexMapView
from Map import HexMap, CubeInd
from Ship import Ship
import unittest
from Settings import ScreenSettings


class TestGame(unittest.TestCase):

    def setUp(self):
        self.map = HexMap(50, 10)
        self.map_view = HexMapView(self.map, (1200, 800), 100)
        self.ship = Ship(None, self.map, speed=1)
        self.game = Game(self.map, self.ship)
        ScreenSettings.game_tick_time = 0.01

    def test_init(self):
        self.assertEqual(0, self.game.turns)

    def test_next_turn(self):
        self.game.end_turn()
        self.assertEqual(1, self.game.turns)

    def test_move_ship(self):
        self.game.move_ship(CubeInd(1, 0, -1))
        self.assertEqual(CubeInd(0, 0, 0), self.game.ship.hex.indices)
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)

    def test_move_ship_restricted_by_range(self):
        self.assertEqual(CubeInd(0, 0, 0), self.game.ship.hex.indices)
        self.game.move_ship(CubeInd(2, 0, -2))
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
        self.game.ending_turn()
        self.assertEqual(0, self.game.turns)
        self.assertEqual(GameState.ENDING_TURN, self.game.state)
        self.game.wait_and_tick()
        self.assertEqual(GameState.ACTIVE_TURN, self.game.state)
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
        # in the beginning of turn one the ship shouldn't move, only after endings turn 1 the ship should move again
        self.game.ending_turn()
        self.game.wait_and_tick()
        self.assertEqual(2, self.game.turns)
        self.assertEqual(CubeInd(2, 0, -2), self.game.ship.hex.indices)

    def test_move_again_in_same_turn_no_movement(self):
        self.game.move_ship(CubeInd(2, 0, -2))
        self.assertEqual(CubeInd(0, 0, 0), self.game.ship.hex.indices)
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
        self.game.move_ship(CubeInd(2, 0, -2))
        self.game.wait_and_tick()
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)

    def test_move_again_in_same_turn_sufficient_range_but_not_too_far(self):
        self.ship = Ship(None, self.map, speed=2)
        self.game = Game(HexMap(50, 10), self.ship)
        self.game.move_ship(CubeInd(1, 0, -1))
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
        self.game.move_ship(CubeInd(2, 0, -2))
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(2, 0, -2), self.game.ship.hex.indices)
        self.game.move_ship(CubeInd(3, 0, -3))
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(2, 0, -2).s, self.game.ship.hex.indices.s)

    def test_move_ship_during_ending_turn_shouldnt_change_course(self):
        self.ship = Ship(None, self.map, speed=2)
        self.game = Game(HexMap(50, 10), self.ship)
        self.game.move_ship(CubeInd(2, 0, -2))
        self.game.ending_turn()
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
        self.game.move_ship(CubeInd(1, 1, -2))
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(2, 0, -2), self.game.ship.hex.indices)

    def test_end_turn_moves_ship_and_reevaluates_numbers_of_turns_on_hexes(self):
        self.ship = Ship(None, self.map, speed=2)
        self.game = Game(HexMap(50, 10), self.ship)
        self.game.move_ship(CubeInd(4, 0, -4))
        self.assertEqual(1, self.map.get_hex(CubeInd(4, 0, -4)).path_repr[1])
        self.game.ending_turn()
        self.game.wait_and_tick()
        self.game.wait_and_tick()
        self.assertEqual(CubeInd(2, 0, -2), self.game.ship.hex.indices)
        self.assertEqual(0, self.map.get_hex(CubeInd(4, 0, -4)).path_repr[1])

    def test_tick_should_only_fire_after_a_certain_time(self):
        ScreenSettings.game_tick_time = 0.25
        self.game.move_ship(CubeInd(1, 0, -1))
        self.assertEqual(CubeInd(0, 0, 0), self.game.ship.hex.indices)
        self.game.tick()
        self.assertEqual(CubeInd(0, 0, 0), self.game.ship.hex.indices)
        time.sleep(ScreenSettings.game_tick_time)
        self.game.tick()
        self.assertEqual(CubeInd(1, 0, -1), self.game.ship.hex.indices)
