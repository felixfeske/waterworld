import unittest
from Map import HexMap, CubeInd
from Map import HexMapView
from pygame import Vector2


class TestHexMapView(unittest.TestCase):

    def setUp(self):
        self.hex_map = HexMap(50, 5)
        self.hex_map_view = HexMapView(self.hex_map, (150, 120), 0)

    def test_init(self):
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, 0, 0)), self.hex_map_view.hex_views[0].hex)
        self.assertEqual(Vector2(-75, -60), self.hex_map_view.offset)
        self.assertEqual(Vector2(0, 0), self.hex_map_view.center)

    def test_move_x_positive(self):
        self.assertEqual(Vector2(-75, -60), self.hex_map_view.offset)
        self.assertEqual(Vector2(0, 0), self.hex_map_view.center)
        self.assertEqual(Vector2(75, 60), self.hex_map_view.hex_views[0].draw_center)
        self.hex_map_view.move(Vector2(10, 0))
        self.assertEqual(Vector2(-65, -60), self.hex_map_view.offset)
        self.assertEqual(Vector2(10, 0), self.hex_map_view.center)
        self.assertEqual(Vector2(65, 60), self.hex_map_view.hex_views[0].draw_center)

    def test_active_hex(self):
        self.assertEqual(0, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)
        self.hex_map_view.active_hex(Vector2(75, 60))
        self.assertEqual(1, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)

    def test_active_hex_only_one_hex(self):
        self.assertEqual(0, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)
        self.hex_map_view.active_hex(Vector2(75, 60))
        self.assertEqual(1, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)
        self.hex_map_view.active_hex(Vector2(150, 104))
        self.assertEqual(1, self.hex_map.get_hex(CubeInd(1, 0, -1)).status)
        self.assertEqual(0, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)

    def test_move_center_into_new_hex(self):
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, 0, 0)), self.hex_map_view.hex_views[0].hex)
        self.assertEqual(self.hex_map.get_hex(CubeInd(-1, 0, 1)), self.hex_map_view.hex_views[-1].hex)
        self.hex_map_view.move(Vector2(75, 44))
        self.assertEqual(self.hex_map.get_hex(CubeInd(1, 0, -1)), self.hex_map_view.hex_views[0].hex)
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, 0, 0)), self.hex_map_view.hex_views[-1].hex)


class TestHexMapViewBoundary(unittest.TestCase):

    def setUp(self):
        self.hex_map = HexMap(50, 5)
        self.hex_map_view = HexMapView(self.hex_map, (50, 20), 50)

    def test_init(self):
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, 0, 0)), self.hex_map_view.hex_views[0].hex)
        self.assertEqual(Vector2(-25, -10), self.hex_map_view.offset)

    def test_move_x_positive(self):
        self.assertEqual(Vector2(-25, -10), self.hex_map_view.offset)
        self.assertEqual(Vector2(25, 10), self.hex_map_view.hex_views[0].draw_center)
        self.hex_map_view.move(Vector2(10, 0))
        self.assertEqual(Vector2(-15, -10), self.hex_map_view.offset)
        self.assertEqual(Vector2(15, 10), self.hex_map_view.hex_views[0].draw_center)

    def test_active_hex(self):
        self.assertEqual(0, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)
        self.hex_map_view.active_hex(Vector2(25, 10))
        self.assertEqual(1, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)

    def test_active_hex_only_one_hex(self):
        self.assertEqual(0, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)
        self.hex_map_view.active_hex(Vector2(25, 10))
        self.assertEqual(1, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)
        self.hex_map_view.active_hex(Vector2(100, 54))
        self.assertEqual(1, self.hex_map.get_hex(CubeInd(1, 0, -1)).status)
        self.assertEqual(0, self.hex_map.get_hex(CubeInd(0, 0, 0)).status)

    def test_active_hex_over_border(self):
        self.hex_map = HexMap(50, 1)
        self.hex_map_view = HexMapView(self.hex_map, (50, 20), 50)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map_view.last_active_indices)
        self.hex_map_view.active_hex(Vector2(150, 88))
        self.assertEqual(None, self.hex_map_view.last_active_indices)


if __name__ == '__main__':
    unittest.main()
