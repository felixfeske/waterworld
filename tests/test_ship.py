import unittest
from Map import HexMap, CubeInd, Sector, PathPart
from Map import HexMapView
from pygame import Vector2
from Ship import Ship
from Settings import ScreenSettings


class TestShip(unittest.TestCase):

    def setUp(self):
        self.settings = ScreenSettings()
        self.map = HexMap(50, 10)
        self.map_view = HexMapView(self.map, Vector2(self.settings.screen_width, self.settings.screen_height), 0)
        self.ship = Ship(None, self.map, 1000)

    def test_init(self):
        self.assertEqual(self.ship.hex, self.map.get_hex(CubeInd(0, 0, 0)))
        self.assertEqual(self.ship.hex.indices, CubeInd(0, 0, 0))

    def test_set_course(self):
        self.assertEqual(self.ship.hex, self.map.get_hex(CubeInd(0, 0, 0)))
        self.ship.set_target_to_ind(CubeInd(1, 0, -1))
        self.assertEqual(self.ship.hex, self.map.get_hex(CubeInd(0, 0, 0)))
        self.assertEqual(self.ship.hex.indices, CubeInd(0, 0, 0))
        self.assertEqual(Sector.ZERO, self.ship.sectors[0])
        self.ship.take_next_step()
        self.assertEqual([], self.ship.sectors)
        self.assertEqual(self.ship.hex.indices, CubeInd(1, 0, -1))
        self.assertEqual(self.ship.hex, self.map.get_hex(CubeInd(1, 0, -1)))
        self.assertEqual(self.ship.image, self.ship.image_0)

    def test_pos_after_moving_map(self):
        self.assertEqual(self.map_view.offset, Vector2(-600, -400))
        self.map_view.move(Vector2(3 / 2 * self.map.hex_side_length, self.map.hex_height))  # (1, 0, -1) hex
        self.map_view.active_hex(Vector2(0, 0))
        self.ship.set_target_to_ind(CubeInd(1, 0, -1))
        self.ship.take_next_step()
        self.assertEqual(self.map_view.offset, Vector2(-600 + 3 / 2 * self.map.hex_side_length, -400 + self.map.hex_height))
        self.assertEqual(self.ship.hex, self.map.get_hex(CubeInd(1, 0, -1)))


class TestShipRange(unittest.TestCase):

    def setUp(self):
        self.settings = ScreenSettings()
        self.map = HexMap(50, 10)
        self.map_view = HexMapView(self.map, Vector2(self.settings.screen_width, self.settings.screen_height), 0)
        self.ship = Ship(None, self.map, 1)

    def test_move_ship(self):
        self.ship.set_target_to_ind(CubeInd(1, 0, -1))  # (0, 1) hex
        self.assertEqual(CubeInd(0, 0, 0), self.ship.hex.indices)
        self.ship.take_next_step()
        self.assertEqual(CubeInd(1, 0, -1), self.ship.hex.indices)

    def test_reset_counter(self):
        self.ship.available_moves -= 1
        self.assertEqual(0, self.ship.available_moves)
        self.ship.reset_move_counter()
        self.assertEqual(1, self.ship.available_moves)
        self.assertEqual(1, self.ship.range)
        self.ship.available_moves -= 1
        self.assertEqual(1, self.ship.range)

    def test_move_ship_restricted_by_range(self):
        self.ship.set_target_to_ind(CubeInd(2, 0, -2))
        self.assertEqual(CubeInd(0, 0, 0), self.ship.hex.indices)
        self.assertEqual(1, self.ship.available_moves)
        self.ship.take_next_step()
        self.assertEqual(0, self.ship.available_moves)
        self.assertEqual(CubeInd(1, 0, -1), self.ship.hex.indices)
        self.ship.take_next_step()
        self.assertEqual(CubeInd(1, 0, -1), self.ship.hex.indices)
        self.ship.reset_move_counter()
        self.ship.activated_for_movement = True
        self.assertEqual(1, self.ship.available_moves)
        self.ship.take_next_step()
        self.assertEqual(CubeInd(2, 0, -2), self.ship.hex.indices)

    def test_move_again_in_same_turn_no_movement(self):
        self.ship.set_target_to_ind(CubeInd(2, 0, -2))  # (1, 2) hex
        self.ship.take_next_step()
        self.assertEqual(CubeInd(1, 0, -1), self.ship.hex.indices)
        self.ship.set_target_to_ind(CubeInd(2, 0, -2))  # (1, 2) hex
        self.ship.take_next_step()
        self.assertEqual(CubeInd(1, 0, -1), self.ship.hex.indices)


class TestShipPath(unittest.TestCase):

    def setUp(self):
        self.settings = ScreenSettings()
        self.map = HexMap(50, 10)
        self.map_view = HexMapView(self.map, Vector2(self.settings.screen_width, self.settings.screen_height), 0)
        self.ship = Ship(None, self.map, 2)

    def test_hexes_are_part_of_path(self):
        self.ship.set_target_to_ind(CubeInd(2, 0, -2))
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(2, 0, -2)).path_part)

    def test_hexes_get_deleted_from_path(self):
        self.ship.set_target_to_ind(CubeInd(2, 0, -2))
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.ship.set_target_to_ind(CubeInd(0, 2, -2))
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(0, 1, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(0, 2, -2)).path_part)
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(2, 0, -2)).path_part)

    def test_hexes_get_deleted_from_path_after_ship_moves_over(self):
        self.ship.set_target_to_ind(CubeInd(2, 0, -2))
        self.assertEqual(self.map.get_hex(CubeInd(0, 0, 0)), self.ship.hex)
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.ship.take_next_step()
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.ship.take_next_step()
        self.assertEqual(self.map.get_hex(CubeInd(2, 0, -2)), self.ship.hex)
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(2, 0, -2)).path_part)

    def test_turns_are_written(self):
        self.ship.set_target_to_ind(CubeInd(4, 0, -4))
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(3, 0, -3)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(4, 0, -4)).path_part)
        self.assertEqual(None, self.map.get_hex(CubeInd(0, 0, 0)).path_repr[1])
        self.assertEqual(0, self.map.get_hex(CubeInd(2, 0, -2)).path_repr[1])
        self.assertEqual(1, self.map.get_hex(CubeInd(4, 0, -4)).path_repr[1])

    def test_turns_reevaluated(self):
        self.ship.set_target_to_ind(CubeInd(4, 0, -4))  # (2, 4) hex
        self.ship.take_next_step()
        self.ship.take_next_step()
        self.assertEqual(self.ship.hex, self.map.get_hex(CubeInd(2, 0, -2)))
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.assertEqual(Sector.ZERO, self.map.get_hex(CubeInd(2, 0, -2)).path_repr[2])
        self.assertEqual(None, self.map.get_hex(CubeInd(0, 0, 0)).path_repr[1])
        self.assertEqual(None, self.map.get_hex(CubeInd(1, 0, -1)).path_repr[1])
        self.assertEqual(1, self.map.get_hex(CubeInd(4, 0, -4)).path_repr[1])
        self.ship.reset_move_counter()
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.assertEqual(Sector.ZERO, self.map.get_hex(CubeInd(2, 0, -2)).path_repr[2])
        self.assertEqual(None, self.map.get_hex(CubeInd(1, 0, -1)).path_repr[1])
        self.assertEqual(None, self.map.get_hex(CubeInd(2, 0, -2)).path_repr[1])
        self.assertEqual(0, self.map.get_hex(CubeInd(4, 0, -4)).path_repr[1])

    def test_turns_are_written_move_points_expire(self):
        self.ship.set_target_to_ind(CubeInd(3, 0, -3))  # (1, 3) hex
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(2, 0, -2)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(3, 0, -3)).path_part)
        self.assertEqual(None, self.map.get_hex(CubeInd(1, 0, -1)).path_repr[1])
        self.assertEqual(0, self.map.get_hex(CubeInd(2, 0, -2)).path_repr[1])
        self.assertEqual(1, self.map.get_hex(CubeInd(3, 0, -3)).path_repr[1])

    def test_path_is_correct(self):
        self.ship.set_target_to_ind(CubeInd(1, 1, -2))
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.MIDDLE, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(1, 1, -2)).path_part)
        self.ship.take_next_step()
        self.assertEqual(PathPart.NO_PART, self.map.get_hex(CubeInd(0, 0, 0)).path_part)
        self.assertEqual(PathPart.BEGINNING, self.map.get_hex(CubeInd(1, 0, -1)).path_part)
        self.assertEqual(PathPart.END, self.map.get_hex(CubeInd(1, 1, -2)).path_part)
        self.assertEqual(None, self.map.get_hex(CubeInd(1, 0, -1)).path_repr[0])
        self.assertEqual(Sector.ONE, self.map.get_hex(CubeInd(1, 0, -1)).path_repr[2])
        self.assertEqual(Sector.FOUR, self.map.get_hex(CubeInd(1, 1, -2)).path_repr[0])
        self.assertEqual(None, self.map.get_hex(CubeInd(1, 1, -2)).path_repr[2])
