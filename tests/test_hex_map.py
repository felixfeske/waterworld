from unittest import TestCase
from Map import CubeInd, HexMap, Sector, Hexagon
from pygame import Vector2


class TestHexMap(TestCase):

    def setUp(self):
        self.hex_map = HexMap(50, 1)

    def test_init(self):
        self.assertEqual(50, self.hex_map.hex_side_length)
        self.assertEqual(7, len(self.hex_map.hexes))
        self.assertEqual(Hexagon(Vector2(0, 0), 50, CubeInd(0, 0, 0)), self.hex_map.get_hex(CubeInd(0, 0, 0)))
        self.assertEqual(Hexagon(Vector2(3/2 * self.hex_map.hex_side_length, self.hex_map.hex_height), 50, CubeInd(1, 0, -1)), self.hex_map.get_hex(CubeInd(1, 0, -1)))
        self.assertEqual(Hexagon(Vector2(0, 2 * self.hex_map.hex_height), 50, CubeInd(0, 1, -1)), self.hex_map.get_hex(CubeInd(0, 1, -1)))
        self.assertEqual(Hexagon(Vector2(- 3/2 * self.hex_map.hex_side_length, self.hex_map.hex_height), 50, CubeInd(-1, 1, 0)), self.hex_map.get_hex(CubeInd(-1, 1, 0)))
        self.assertEqual(Hexagon(Vector2(- 3/2 * self.hex_map.hex_side_length, - self.hex_map.hex_height), 50, CubeInd(-1, 0, 1)), self.hex_map.get_hex(CubeInd(-1, 0, 1)))
        self.assertEqual(Hexagon(Vector2(0, - 2 * self.hex_map.hex_height), 50, CubeInd(0, -1, 1)), self.hex_map.get_hex(CubeInd(0, -1, 1)))
        self.assertEqual(Hexagon(Vector2(3/2 * self.hex_map.hex_side_length, - self.hex_map.hex_height), 50, CubeInd(1, -1, 0)), self.hex_map.get_hex(CubeInd(1, -1, 0)))

    def test_pixel_to_hex(self):
        coords = Vector2(0, 0)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        coords = Vector2(0, 1)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        coords = Vector2(0, 43)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        coords = Vector2(25, 0)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        coords = Vector2(37, 21)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        coords = Vector2(37, -21)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        # sector 2
        coords = Vector2(-37, 21)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        # sector 3
        coords = Vector2(-37, -21)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))
        coords = Vector2(-38, -22)
        self.assertNotEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        # sector 4
        coords = Vector2(1, -43)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))
        coords = Vector2(1, -44)
        self.assertNotEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))

        coords = Vector2(-50, 0)
        self.assertEqual(CubeInd(0, 0, 0), self.hex_map.coords_to_ind(coords))
        self.assertEqual(self.hex_map.get_hex(CubeInd(1, 0, -1)).center, self.hex_map.ind_to_coords(CubeInd(1, 0, -1)))

    def test_indices_over_border(self):
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(2, 0, -2)))
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(6, -3, -3)))
        self.assertFalse(self.hex_map.indices_over_border(CubeInd(1, 0, -1)))
        self.assertFalse(self.hex_map.indices_over_border(CubeInd(0, 0, 0)))

    def test_move(self):
        self.hex_map = HexMap(50, 3)
        hexagon = self.hex_map.get_hex(CubeInd(0, 0, 0))
        self.assertEqual(self.hex_map.get_hex(CubeInd(1, 0, -1)), self.hex_map.move(hexagon, sector=Sector.ZERO))
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, 1, -1)), self.hex_map.move(hexagon, sector=Sector.ONE))
        self.assertEqual(self.hex_map.get_hex(CubeInd(-1, 1, 0)), self.hex_map.move(hexagon, sector=Sector.TWO))
        self.assertEqual(self.hex_map.get_hex(CubeInd(-1, 0, 1)), self.hex_map.move(hexagon, sector=Sector.THREE))
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, -1, 1)), self.hex_map.move(hexagon, sector=Sector.FOUR))
        self.assertEqual(self.hex_map.get_hex(CubeInd(1, -1, 0)), self.hex_map.move(hexagon, sector=Sector.FIVE))

    def test_move_steps(self):
        self.hex_map = HexMap(50, 2)
        self.assertEqual(19, len(self.hex_map.hexes.values()))
        hexagon = self.hex_map.get_hex(CubeInd(0, 0, 0))
        self.assertEqual(self.hex_map.get_hex(CubeInd(0, 0, 0)), self.hex_map.move_steps(hexagon, Sector.ZERO, 0))
        self.assertEqual(self.hex_map.get_hex(CubeInd(2, 0, -2)), self.hex_map.move_steps(hexagon, Sector.ZERO, 2))

    def test_ring_0(self):
        self.assertEqual([self.hex_map.get_hex(CubeInd(0, 0, 0))], self.hex_map.ring(self.hex_map.get_hex(CubeInd(0, 0, 0)), 0))

    def test_ring_1(self):
        expected_ring = [self.hex_map.get_hex(CubeInd(0, -1, 1)),
                         self.hex_map.get_hex(CubeInd(1, -1, 0)),
                         self.hex_map.get_hex(CubeInd(1, 0, -1)),
                         self.hex_map.get_hex(CubeInd(0, 1, -1)),
                         self.hex_map.get_hex(CubeInd(-1, 1, 0)),
                         self.hex_map.get_hex(CubeInd(-1, 0, 1))]
        self.assertEqual(expected_ring, self.hex_map.ring(self.hex_map.get_hex(CubeInd(0, 0, 0)), 1))

    def test_ring_2(self):
        self.hex_map = HexMap(50, 2)
        expected_ring = [self.hex_map.get_hex(CubeInd(0, -2, 2)),
                         self.hex_map.get_hex(CubeInd(1, -2, 1)),
                         self.hex_map.get_hex(CubeInd(2, -2, 0)),
                         self.hex_map.get_hex(CubeInd(2, -1, -1)),
                         self.hex_map.get_hex(CubeInd(2, 0, -2)),
                         self.hex_map.get_hex(CubeInd(1, 1, -2)),
                         self.hex_map.get_hex(CubeInd(0, 2, -2)),
                         self.hex_map.get_hex(CubeInd(-1, 2, -1)),
                         self.hex_map.get_hex(CubeInd(-2, 2, 0)),
                         self.hex_map.get_hex(CubeInd(-2, 1, 1)),
                         self.hex_map.get_hex(CubeInd(-2, 0, 2)),
                         self.hex_map.get_hex(CubeInd(-1, -1, 2))]
        self.assertEqual(expected_ring, self.hex_map.ring(self.hex_map.get_hex(CubeInd(0, 0, 0)), 2))

    def test_circle(self):
        expected_circle = [self.hex_map.get_hex(CubeInd(0, 0, 0)),
                           self.hex_map.get_hex(CubeInd(0, -1, 1)),
                           self.hex_map.get_hex(CubeInd(1, -1, 0)),
                           self.hex_map.get_hex(CubeInd(1, 0, -1)),
                           self.hex_map.get_hex(CubeInd(0, 1, -1)),
                           self.hex_map.get_hex(CubeInd(-1, 1, 0)),
                           self.hex_map.get_hex(CubeInd(-1, 0, 1))]
        self.assertEqual(expected_circle, self.hex_map.circle(self.hex_map.get_hex(CubeInd(0, 0, 0)), 1))

    def test_find_path_to_coords_trivial(self):
        self.assertEqual({'path': [], 'sectors': []}, self.hex_map.find_path_to_coords(CubeInd(0, 0, 0), Vector2(0, 0)))

    def test_find_path_to_coords_one_step(self):
        self.assertEqual({'path': [self.hex_map.get_hex(CubeInd(1, 0, -1))], 'sectors': [Sector.ZERO]}, self.hex_map.find_path_to_coords(CubeInd(0, 0, 0), Vector2(75, 44)))

    def test_find_path_to_coords_two_steps(self):
        self.hex_map = HexMap(50, 2)
        self.assertEqual({'path': [self.hex_map.get_hex(CubeInd(1, 0, -1)), self.hex_map.get_hex(CubeInd(2, 0, -2))],
                          'sectors': [Sector.ZERO, Sector.ZERO]}, self.hex_map.find_path_to_coords(CubeInd(0, 0, 0), Vector2(150, 88)))

    def test_over_border(self):
        self.hex_map = HexMap(50, 0)
        self.assertFalse(self.hex_map.indices_over_border(CubeInd(0, 0, 0)))
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(1, 0, -1)))
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(0, 1, -1)))
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(-1, 1, 0)))
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(-1, 0, 1)))
        self.assertTrue(self.hex_map.indices_over_border(CubeInd(0, -1, 1)))


class TestCubeInd(TestCase):

    def test_init(self):
        self.cube = CubeInd(0, 0, 0)
        self.assertEqual(self.cube, CubeInd.qr(0, 0))
        self.cube = CubeInd(1, 0, -1)
        self.assertEqual(self.cube, CubeInd.qr(1, 0))
        self.assertEqual(self.cube, CubeInd.rs(0, -1))
        self.assertEqual(self.cube, CubeInd.qs(1, -1))

    def test_rounding(self):
        self.cube = CubeInd(0.1, 0, 0)
        self.assertEqual(0, self.cube.q)
        self.cube = CubeInd(0.6, 0, 0)
        self.assertEqual(0, self.cube.q)
        self.cube = CubeInd(0.6, 0, 0.7)
        self.assertEqual(CubeInd(-1, 0, 1), self.cube)

    def test_add(self):
        self.cube0 = CubeInd(1, -1, 0)
        self.cube1 = CubeInd.qr(-1, 1)
        self.assertEqual(CubeInd(0, 0, 0), self.cube0 + self.cube1)
