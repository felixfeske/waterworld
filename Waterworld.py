import pygame
from Game import GameController, GameView, Game
from Settings import ScreenSettings, MapSettings
from Ship import Ship
from pygame import Vector2
from Map import HexMap, HexMapView


def run_game():
    # Initialize pygame, settings and screen object
    pygame.init()
    grid = HexMap(MapSettings.hex_side_length, 30)
    map_view = HexMapView(grid, Vector2(ScreenSettings.screen_width, ScreenSettings.screen_height), 50)
    screen = pygame.display.set_mode((ScreenSettings.screen_width, ScreenSettings.screen_height))
    ship = Ship(screen, grid, 4)
    game = Game(grid, ship)
    controller = GameController(map_view, ship, game)
    view = GameView(screen, map_view, game)
    pygame.display.set_caption("Waterworld")
    fps_clock = pygame.time.Clock()
    fps = 30

    while True:
        fps_clock.tick(fps)

        controller.check_events()
        controller.update_model()
        view.update_screen()


run_game()
