from pygame import Vector2


class MapSettings:
        hex_side_length = 50
        map_center = Vector2(0, 0)
        border_color = (0, 0, 0)
        active_border_color = (255, 255, 255)
        path_color = (200, 200, 200)
        scrolling_speed = 20
