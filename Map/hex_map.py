from pygame import Vector2
import math
from Map import Hexagon, Sector, CubeInd


class HexMap:

    def __init__(self, hex_side_length, radius):

        self.hex_side_length = hex_side_length
        self.hex_height = math.sqrt(self.hex_side_length ** 2 - (self.hex_side_length / 2) ** 2)
        self.grid_radius = radius
        self.hexes = {}
        self.init_hexes()

    def init_hexes(self):
        for q in range(-self.grid_radius, self.grid_radius + 1):
            for r in range(max(-self.grid_radius, -q-self.grid_radius), min(-q + self.grid_radius, self.grid_radius)+1):
                self.hexes[CubeInd.qr(q, r)] = Hexagon(Vector2(3/2 * self.hex_side_length * q, self.hex_height * (q + 2 * r)), self.hex_side_length, CubeInd.qr(q, r))

    def get_hex(self, cube_ind):
        return self.hexes[cube_ind]

    def coords_to_ind(self, coords):
        q = 2/3 * coords.x / self.hex_side_length
        r = 1/2 * (coords.y / self.hex_height - q)
        return CubeInd.qr(q, r)

    def ind_to_coords(self, indices):
        x = 3/2 * self.hex_side_length * indices.q
        y = self.hex_height * (2 * indices.r + indices.q)
        return Vector2(x, y)

    def hex_on_coords(self, coords):
        return self.hexes[self.coords_to_ind(coords)]

    def move(self, start_hex, sector: Sector):
        return self.hexes[start_hex.indices + CubeInd.sector_to_direction(sector)]

    def move_steps(self, start_hex, direction, steps):
        if steps == 0:
            return start_hex
        else:
            return self.move_steps(self.move(start_hex, direction), direction, steps - 1)

    def ring(self, center_hex, radius):
        hex_on_ring = self.move_steps(center_hex, Sector.FOUR, radius)
        ring = [hex_on_ring]
        for direction in Sector:
            if direction is Sector.FIVE:
                for steps in range(radius - 1):
                    hex_on_ring = self.move(hex_on_ring, direction)
                    ring.append(hex_on_ring)
            else:
                for steps in range(radius):
                    hex_on_ring = self.move(hex_on_ring, direction)
                    ring.append(hex_on_ring)
        return ring

    def circle(self, center_hex, radius):
        circle = [center_hex]
        for i in range(1, radius + 1):
            ring = self.ring(center_hex, i)
            circle += ring
        return circle

    def find_path_to_coords(self, start_ind, coords):
        end_ind = self.coords_to_ind(coords)
        return self.find_path_to_ind(start_ind, end_ind)

    def find_path_to_ind(self, start_ind, target_ind):
        current_hex = self.hexes[start_ind]
        visited_hexes = set([])
        hexes_to_search = [(current_hex, [], [])]
        (current_hex, path, sectors) = hexes_to_search.pop(0)
        while current_hex.indices != target_ind:
            for direction in Sector:
                new_hex = self.move(current_hex, direction)
                if new_hex.indices not in visited_hexes:
                    visited_hexes.add(new_hex.indices)
                    hexes_to_search.append((new_hex, path + [new_hex], sectors + [direction]))
            (current_hex, path, sectors) = hexes_to_search.pop(0)
        return {'path': path, 'sectors': sectors}

    def indices_over_border(self, indices):
        return indices.radius() > self.grid_radius
