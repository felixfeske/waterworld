from pygame import Vector2
import math
from Map import PathPart
from Map import Sector


class Hexagon:

    def __init__(self, center, side_length, indices):
        self.side_length = side_length
        self.center = center
        self.status = 0
        self.height = math.sqrt(self.side_length**2 - (self.side_length / 2)**2)
        self.edge_points = self.coordinate_list(0, self.side_length)
        self.tol = 1e-10
        self.indices = indices
        self.path_part = PathPart.NO_PART
        self.path_repr = (None, None, None)
        self.ship = None

    def delete_from_path(self):
        self.path_part = PathPart.NO_PART
        self.path_repr = (None, None, None)

    def make_beginning_of_path(self, turn, to_sector):
        if to_sector is not None:
            self.path_repr = (None, turn, to_sector)
        self.path_part = PathPart.BEGINNING

    def make_middle_of_path(self, from_sector, turn, to_sector):
        self.path_part = PathPart.MIDDLE
        self.path_repr = (from_sector, turn, to_sector)

    def make_end_of_path(self, from_sector, turn):
        self.path_part = PathPart.END
        self.path_repr = (from_sector, turn, None)

    @staticmethod
    def coordinate_list(start, radius):
        angle = [i/6 + start for i in range(0, 6)]
        x = [radius * math.cos(2 * math.pi * w) for w in angle]
        y = [radius * math.sin(2 * math.pi * w) for w in angle]
        return [Vector2(vector[0], vector[1]) for vector in zip(x, y)]

    def spans(self, coords):
        sector = self.sector_from_coords(coords)
        return self.coords_inside_sector(sector, coords - self.center)

    def coords_inside_sector(self, sector, coords):
        (m, b) = self.calc_line_parameters(sector)

        def line(x): return m * x + b
        if (sector == Sector.ZERO or sector == Sector.ONE
                or sector == Sector.TWO):
            if line(coords.x) + self.tol >= coords.y:
                return True
            else:
                return False
        else:
            if line(coords.x) - self.tol <= coords.y:
                return True
            else:
                return False

    def calc_line_parameters(self, sector):
        delta = self.edge_points[(sector.value + 1) % 6] - self.edge_points[sector.value]
        m = delta.y / delta.x
        b = self.edge_points[(sector.value + 1) % 6].y - m * self.edge_points[(sector.value + 1) % 6].x
        return m, b

    def sector_from_coords(self, coords):
        distance = coords - self.center
        alpha = Vector2(100, 0).angle_to(distance)
        sector = math.floor((alpha + 180) / 60)
        if sector == 3:
            return Sector.ZERO
        elif sector == 4:
            return Sector.ONE
        elif sector == 5:
            return Sector.TWO
        elif sector == 0:
            return Sector.THREE
        elif sector == 6:
            return Sector.TWO
        elif sector == 1:
            return Sector.FOUR
        elif sector == 2:
            return Sector.FIVE

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.center == other.center) and (self.side_length == other.side_length) and (self.indices == other.indices)
        else:
            return False
