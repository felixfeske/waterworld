from Map import Sector


class CubeInd:

    def __init__(self, q, r, s):
        self.q = round(q)
        self.r = round(r)
        self.s = round(s)

        q_diff = abs(q - self.q)
        r_diff = abs(r - self.r)
        s_diff = abs(s - self.s)

        if self.r + self.q + self.s != 0:
            if q_diff > r_diff and q_diff > s_diff:
                self.q = - self.r - self.s
            elif r_diff > s_diff:
                self.r = -self.q - self.s
            else:
                self.s = -self.q - self.r

        assert self.r + self.q + self.s == 0

    @classmethod
    def qr(cls, q, r):
        return cls(q, r, - r - q)

    @classmethod
    def qs(cls, q, s):
        return cls(q, - q - s, s)

    @classmethod
    def rs(cls, r, s):
        return cls(-r - s, r, s)

    @classmethod
    def rqs(cls, r, q, s):
        return cls(r, q, s)

    def __hash__(self):
        return hash((self.q, self.r, self.s))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.r == other.r) and (self.q == other.q) and (self.s == other.s)
        else:
            return False

    def __add__(self, other):
        return CubeInd(self.q + other.q, self.r + other.r, self.s + other.s)

    def __mul__(self, other):
        if isinstance(other, int):
            return CubeInd(other * self.q, other * self.r, other * self.s)
        else:
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, int):
            return CubeInd(other * self.q, other * self.r, other * self.s)
        else:
            return NotImplemented

    def rotate120(self):
        tempq = self.q
        self.q = self.r
        self.r = self.s
        self.s = tempq

    def rotate60(self):
        temps = self.s
        self.s = - self.r
        self.r = - self.q
        self.q = - temps

    @classmethod
    def sector_to_direction(cls, sector: Sector):
        return CubeInd(0, 0, 0).move(sector)

    def move(self, sector: Sector):
        if sector is Sector.ZERO:
            return self + CubeInd(1, 0, -1)
        elif sector is Sector.ONE:
            return self + CubeInd(0, 1, -1)
        elif sector is Sector.TWO:
            return self + CubeInd(-1, 1, 0)
        elif sector is Sector.THREE:
            return self + CubeInd(-1, 0, 1)
        elif sector is Sector.FOUR:
            return self + CubeInd(0, -1, 1)
        elif sector is Sector.FIVE:
            return self + CubeInd(1, -1, 0)

    def radius(self):
        return max(abs(self.q), abs(self.r), abs(self.s))
