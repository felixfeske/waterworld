from pygame import Vector2
from Map import CubeInd, HexagonView
from Settings import MapSettings


class HexMapView:

    def __init__(self, hex_map, screen_dim, boundary):
        self.hex_map = hex_map
        self.screen_dim = screen_dim
        self.boundary = boundary
        self.radius = self.calculate_radius(screen_dim, boundary)
        self.offset = Vector2(- screen_dim[0] / 2, - screen_dim[1] / 2)
        self.hex_views = [HexagonView(hexagon, self.offset * (-1)) for hexagon in self.hex_map.circle(self.hex_map.get_hex(CubeInd(0, 0, 0)), self.radius)]
        self.center_ind = CubeInd(0, 0, 0)
        self.center = Vector2(0, 0)
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False
        self.last_active_indices = CubeInd(0, 0, 0)

    def draw_map(self, screen):
        for hex_view in self.hex_views:
            hex_view.draw_hex(screen)

    def calculate_radius(self, screen_dim, boundary):
        ind = self.hex_map.coords_to_ind(Vector2(screen_dim[0] / 2 + boundary, screen_dim[1] / 2 + boundary))
        return max(abs(ind.q), abs(ind.r), abs(ind.s))

    def move(self, coords):
        temp_offset = self.offset + coords
        temp_center = self.center + coords
        new_ind = self.hex_map.coords_to_ind(temp_center)
        if self.hex_map.indices_over_border(new_ind):
            return
        else:
            self.center = temp_center
            self.offset = temp_offset
            if new_ind != self.center_ind:
                self.center_ind = self.hex_map.coords_to_ind(self.center)
                self.hex_views = [HexagonView(hexagon, -self.offset) for hexagon in self.hex_map.circle(self.hex_map.get_hex(self.center_ind), self.radius)]
            else:
                for hex_view in self.hex_views:
                    hex_view.update(coords)

    def update_map(self, mouse_pos):
        self.active_hex(Vector2(mouse_pos[0], mouse_pos[1]))
        if self.moving_left or mouse_pos[0] == 0:
            self.move(Vector2(-MapSettings.scrolling_speed, 0))
        if self.moving_right or mouse_pos[0] == self.screen_dim.x - 1:
            self.move(Vector2(MapSettings.scrolling_speed, 0))
        if self.moving_up or mouse_pos[1] == 0:
            self.move(Vector2(0, -MapSettings.scrolling_speed))
        if self.moving_down or mouse_pos[1] == self.screen_dim.y - 1:
            self.move(Vector2(0, MapSettings.scrolling_speed))

    def active_hex(self, coords):
        clicked_indices = self.hex_map.coords_to_ind(coords + self.offset)
        if self.last_active_indices is not None:
            self.hex_map.get_hex(self.last_active_indices).status = 0
        if self.hex_map.indices_over_border(clicked_indices):
            self.last_active_indices = None
        else:
            self.last_active_indices = clicked_indices
            self.hex_map.get_hex(self.last_active_indices).status = 1
