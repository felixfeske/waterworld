from Settings import MapSettings, ScreenSettings
from pygame import Vector2
import math
import pygame
from Map import PathPart


class HexagonView:

    def __init__(self, hexagon, offset):
        self.hex = hexagon
        self.status = 0
        self.draw_center = Vector2(hexagon.center.x + offset.x, hexagon.center.y + offset.y)
        self.draw_points = self.coordinate_list(self.hex.side_length, 0, 7, 3)
        self.draw_points_2 = self.coordinate_list(self.hex.side_length, 0, 7, 2)
        self.mid_points = self.coordinate_list(self.hex.height, 1 / 6 * math.pi, 6, 0)

    def coordinate_list(self, radius, start_angle, number_points, inset):
        tmp_list = []
        for i in range(number_points):
            tmp_list.append(Vector2((radius - inset) * math.cos(math.pi / 3 * i + start_angle) + self.draw_center.x,
                                    (radius - inset) * math.sin(math.pi / 3 * i + start_angle) + self.draw_center.y))
        return tmp_list

    def update(self, offset):
        self.draw_center -= offset
        for i in range(len(self.draw_points)):
            self.draw_points[i] -= offset
            self.draw_points_2[i] -= offset
        for i in range(len(self.mid_points)):
            self.mid_points[i] -= offset

    def is_active(self):
        return self.hex.status == 1

    def draw_hex(self, screen):
        if self.is_active():
            self.draw_border(screen, MapSettings.active_border_color)
        else:
            self.draw_border(screen, MapSettings.border_color)

        if self.hex.path_part is PathPart.BEGINNING:
            pygame.draw.line(screen, MapSettings.path_color, self.draw_center, self.mid_points[self.hex.path_repr[2].value], 3)
        elif self.hex.path_part is PathPart.MIDDLE:
            pygame.draw.line(screen, MapSettings.path_color, self.draw_center, self.mid_points[self.hex.path_repr[0].value], 3)
            pygame.draw.line(screen, MapSettings.path_color, self.draw_center, self.mid_points[self.hex.path_repr[2].value], 3)
            if self.hex.path_repr[1] is not None:
                pygame.draw.circle(screen, ScreenSettings.bg_color, (int(self.draw_center.x), int(self.draw_center.y)), 12)
                pygame.draw.circle(screen, MapSettings.path_color, (int(self.draw_center.x), int(self.draw_center.y)), 12, int(2))
                self.message_display(screen, str(self.hex.path_repr[1]), self.draw_center)
        elif self.hex.path_part is PathPart.END:
            pygame.draw.line(screen, MapSettings.path_color, self.draw_center, self.mid_points[self.hex.path_repr[0].value], 3)
            pygame.draw.circle(screen, ScreenSettings.bg_color, (int(self.draw_center.x), int(self.draw_center.y)), 12)
            pygame.draw.circle(screen, MapSettings.path_color, (int(self.draw_center.x), int(self.draw_center.y)), 12, int(2))
            self.message_display(screen, str(self.hex.path_repr[1]), self.draw_center)

        if self.hex.ship is not None:
            self.hex.ship.blit_me(self.draw_center)

    def draw_border(self, screen, color):
        pygame.draw.aalines(screen, color, True, self.draw_points)
        pygame.draw.aalines(screen, color, True, self.draw_points_2)

    @staticmethod
    def text_objects(text, font):
        text_surface = font.render(text, True, MapSettings.path_color)
        return text_surface, text_surface.get_rect()

    def message_display(self, screen, text, pos):
        large_text = pygame.font.Font(None, 20)
        text_surf, text_rect = self.text_objects(text, large_text)
        text_rect.center = pos
        screen.blit(text_surf, text_rect)

