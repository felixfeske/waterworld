from enum import Enum


class PathPart(Enum):
    NO_PART = 0
    END = 1
    MIDDLE = 2
    BEGINNING = 3
