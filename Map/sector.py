from enum import Enum


class Sector(Enum):
    ZERO = 0
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5

    def opposite(self):
        if self is Sector.ZERO:
            return Sector.THREE
        elif self is Sector.ONE:
            return Sector.FOUR
        elif self is Sector.TWO:
            return Sector.FIVE
        elif self is Sector.THREE:
            return Sector.ZERO
        elif self is Sector.FOUR:
            return Sector.ONE
        elif self is Sector.FIVE:
            return Sector.TWO

    def previous(self):
        if self is Sector.ZERO:
            return Sector.FIVE
        elif self is Sector.ONE:
            return Sector.ZERO
        elif self is Sector.TWO:
            return Sector.ONE
        elif self is Sector.THREE:
            return Sector.TWO
        elif self is Sector.FOUR:
            return Sector.THREE
        elif self is Sector.FIVE:
            return Sector.FOUR
