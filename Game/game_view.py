import pygame
from pygame import Vector2
from Settings import ScreenSettings


class GameView:

    def __init__(self, screen, grid_view, game):
        self.screen = screen
        self.grid_view = grid_view
        self.game = game

    def update_screen(self):
        self.grid_view.update_map(pygame.mouse.get_pos())
        # Redraw the screen during each pass through the loop
        self.screen.fill(ScreenSettings.bg_color)
        self.grid_view.draw_map(self.screen)
        self.print_turns()
        self.print_available_moves()
        # Make the most recently drawn screen visible
        pygame.display.flip()

    def print_turns(self):
        self.message_display(self.screen, "Turn: " + str(self.game.turns), Vector2(ScreenSettings.screen_width - 50, 20))

    def print_available_moves(self):
        self.message_display(self.screen, "Moves: " + str(self.game.ship.available_moves), Vector2(ScreenSettings.screen_width - 50, 50))

    @staticmethod
    def text_objects(text, font):
        text_surface = font.render(text, True, (0, 0, 0))
        return text_surface, text_surface.get_rect()

    def message_display(self, screen, text, pos):
        large_text = pygame.font.Font(None, 30)
        text_surf, text_rect = self.text_objects(text, large_text)
        text_rect.center = (pos.x, pos.y)
        screen.blit(text_surf, text_rect)
