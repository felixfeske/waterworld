import pygame
import sys

right_mouse_button = 3
left_mouse_button = 1


class GameController:

    def __init__(self, grid_view, ship, game):
        self.ship = ship
        self.grid_view = grid_view
        self.game = game

    def update_model(self):
        self.game.tick()

    def check_events(self):
        # Respond to keypress and mouse events.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == left_mouse_button:
                    self.game.move_ship(self.grid_view.last_active_indices)
                elif event.button == right_mouse_button:
                    self.game.ending_turn()
            elif event.type == pygame.KEYDOWN:
                self.check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self.check_keyup_events(event)

    def check_keydown_events(self, event):
        if event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_RIGHT:
            self.grid_view.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.grid_view.moving_left = True
        elif event.key == pygame.K_DOWN:
            self.grid_view.moving_down = True
        elif event.key == pygame.K_UP:
            self.grid_view.moving_up = True

    def check_keyup_events(self, event):
        if event.key == pygame.K_RIGHT:
            self.grid_view.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.grid_view.moving_left = False
        elif event.key == pygame.K_DOWN:
            self.grid_view.moving_down = False
        elif event.key == pygame.K_UP:
            self.grid_view.moving_up = False
        elif event.key == pygame.K_RETURN:
            self.game.ending_turn()
