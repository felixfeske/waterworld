import time
from enum import Enum
from Settings import GameSettings


class Game:

    def __init__(self, grid, ship):
        self.turns = 0
        self.grid = grid
        self.ship = ship
        self.ship_not_moved = True
        self.state = GameState.ACTIVE_TURN
        self.last_tick_time = time.time()

    def ending_turn(self):
        if self.state is GameState.ACTIVE_TURN:
            self.state = GameState.ENDING_TURN
            self.ship.activate_for_movement()

    def end_turn(self):
        self.turns += 1
        self.ship.reset_move_counter()
        self.state = GameState.ACTIVE_TURN
        self.ship.deactivate_for_movement()

    def move_ship(self, active_indices):
        if self.state is GameState.ACTIVE_TURN:
            self.ship.activate_for_movement()
            self.ship.set_target_to_ind(active_indices)

    def tick(self):
        if time.time() - self.last_tick_time > GameSettings.game_tick_time:
            self.ship.take_next_step()
            if self.state is GameState.ENDING_TURN:
                if self.ship.has_no_movement_to_do():
                    self.end_turn()
            self.last_tick_time = time.time()

    def wait_and_tick(self):
        time.sleep(GameSettings.game_tick_time)
        self.tick()


class GameState(Enum):
    ACTIVE_TURN = 0
    ENDING_TURN = 1
