import pygame
from Map import CubeInd, Sector, PathPart, HexMap
import math
from PIL import Image
import os


class Ship:

    def __init__(self, screen: pygame.display, hex_map: HexMap, speed: int):
        self.range = speed
        self.available_moves = speed
        self.screen = screen
        path = os.path.dirname(os.path.realpath(__file__))
        if pygame.image.get_extended:
            self.image_0 = pygame.image.load(path + "/resources/Floß_0.png")
            self.image_1 = pygame.image.load(path + "/resources/Floß_1.png")
            self.image_2 = pygame.image.load(path + "/resources/Floß_2.png")
            self.image_3 = pygame.image.load(path + "/resources/Floß_3.png")
            self.image_4 = pygame.image.load(path + "/resources/Floß_4.png")
            self.image_5 = pygame.image.load(path + "/resources/Floß_5.png")
        else:
            img_0 = Image.open(path + "/resources/Floß_0.png")
            img_0.save(path + "/resources/Floß_0.bmp")
            self.image_0 = pygame.image.load(path + "/resources/Floß_0.bmp")
            img_1 = Image.open(path + "/resources/Floß_1.png")
            img_1.save(path + "/resources/Floß_1.bmp")
            self.image_1 = pygame.image.load(path + "/resources/Floß_1.bmp")
            img_2 = Image.open(path + "/resources/Floß_2.png")
            img_2.save(path + "/resources/Floß_2.bmp")
            self.image_2 = pygame.image.load(path + "/resources/Floß_2.bmp")
            img_3 = Image.open(path + "/resources/Floß_3.png")
            img_3.save(path + "/resources/Floß_3.bmp")
            self.image_3 = pygame.image.load(path + "/resources/Floß_3.bmp")
            img_4 = Image.open(path + "/resources/Floß_4.png")
            img_4.save(path + "/resources/Floß_4.bmp")
            self.image_4 = pygame.image.load(path + "/resources/Floß_4.bmp")
            img_5 = Image.open(path + "/resources/Floß_5.png")
            img_5.save(path + "/resources/Floß_5.bmp")
            self.image_5 = pygame.image.load(path + "/resources/Floß_5.bmp")
        self.image = self.image_4
        self.hex_map = hex_map
        self.rect = self.image.get_rect()
        self.angle = 60
        self.activated_for_movement = True
        self.hex = self.hex_map.get_hex(CubeInd(0, 0, 0))
        self.hex.ship = self
        self.path = []
        self.sectors = []

    def blit_me(self, center: pygame.rect) -> None:
        self.rect.centerx = center.x
        self.rect.centery = center.y
        self.screen.blit(self.image, self.rect)

    def take_next_step(self) -> None:
        if self.available_moves > 0 and self.activated_for_movement:
            if len(self.path) != 0:
                next_hex = self.path.pop(0)
                if self.hex is not None:
                    self.hex.ship = None
                    self.hex.delete_from_path()
                self.hex = next_hex
                self.hex.ship = self
                self.available_moves -= 1
            if len(self.sectors) != 0:
                sector = self.sectors.pop(0)
                if self.hex.path_part is PathPart.END:
                    self.hex.delete_from_path()
                else:
                    self.hex.make_beginning_of_path(None, self.sectors[0])
                self.set_image_according_to_sector(sector)
        else:
            self.activated_for_movement = False

    def reset_move_counter(self) -> None:
        self.available_moves = self.range
        if len(self.path) != 0:
            self.hex.make_beginning_of_path(None, self.sectors[0])
            for i in range(len(self.path) - 1):
                turns_reach_hex = None
                if (i - self.available_moves + 1) % self.range == 0:
                    turns_reach_hex = int((i + 1 - self.available_moves) / self.range)
                self.path[i].make_middle_of_path(self.sectors[i].opposite(), turns_reach_hex, self.sectors[i + 1])
            turns_reach_hex = math.ceil((len(self.path) - self.available_moves) / self.range)
            self.path[-1].make_end_of_path(self.sectors[-1].opposite(), turns_reach_hex)

    def has_no_movement_to_do(self) -> bool:
        return self.available_moves == 0 or len(self.path) == 0

    def activate_for_movement(self) -> None:
        self.activated_for_movement = True

    def deactivate_for_movement(self) -> None:
        self.activated_for_movement = False

    def set_image_according_to_sector(self, sector: Sector) -> None:
        if sector == Sector.ZERO:
            self.image = self.image_0
        elif sector == Sector.ONE:
            self.image = self.image_1
        elif sector == Sector.TWO:
            self.image = self.image_2
        elif sector == Sector.THREE:
            self.image = self.image_3
        elif sector == Sector.FOUR:
            self.image = self.image_4
        elif sector == Sector.FIVE:
            self.image = self.image_5

    def set_target_to_ind(self, target_ind: CubeInd) -> None:
        if len(self.path) != 0:
            for tile in self.path:
                tile.delete_from_path()
        path_dict = self.hex_map.find_path_to_ind(self.hex.indices, target_ind)
        self.path = path_dict['path']
        self.sectors = path_dict['sectors']
        turns_reach_hex = math.ceil((len(self.path) - self.available_moves) / self.range)
        if len(self.path) != 0:
            self.hex.make_beginning_of_path(None, self.sectors[0])
            self.path[-1].make_end_of_path(self.sectors[-1].opposite(), turns_reach_hex)
        for i in range(len(self.path) - 1):
            turns_reach_hex = None
            if (i + 1 - self.available_moves) % self.range == 0:
                turns_reach_hex = int((i + 1 - self.available_moves) / self.range)
            self.path[i].make_middle_of_path(self.sectors[i].opposite(), turns_reach_hex, self.sectors[i + 1])
